package app.admin.controllers;

import app.admin.models.Glass;
import app.admin.services.GlassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GlassController {
    private final GlassService glassService;

    @Autowired
    public GlassController(GlassService glassService) {
        this.glassService = glassService;
    }

    @GetMapping(value = "/glass/{id}")
    public ResponseEntity<Glass> getGlassById(@PathVariable Integer id) {
        Glass gls = glassService.getGlass(id);
        if (gls != null) {
            return new ResponseEntity<>(gls, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/glasses")
    public ResponseEntity<Glass> addGlass(@RequestBody Glass glass) {
        try {
            glassService.addGlass(glass);
            return new ResponseEntity<>(glass, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PatchMapping(value = "/glass/{id}")
    public ResponseEntity<Glass> updateGlass(@PathVariable Integer id, @RequestBody Glass glass) {
        Glass gls = glassService.getGlass(id);

        if (gls == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        gls.setGlass_amount((glass.getGlass_amount() != null) ?
                glass.getGlass_amount() :
                gls.getGlass_amount());

        gls.setGlass_material((glass.getGlass_material() != null) ?
                glass.getGlass_material() :
                gls.getGlass_material());

        glassService.updateGlass(gls);

        return new ResponseEntity<>(gls, HttpStatus.OK);
    }

    @DeleteMapping(value = "/glass/{id}")
    public ResponseEntity<Void> deleteGlass(@PathVariable Integer id) {
        Glass gls = glassService.getGlass(id);

        if (gls == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        glassService.deleteGlass(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
