package app.admin.controllers;

import app.admin.models.Ingredient;
import app.admin.services.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class IngredientController {
    private final IngredientService ingredientService;
    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @GetMapping(value = "/ingredient/{id}")
    public ResponseEntity<Ingredient> getIngredientById(@PathVariable Integer id) {
        Ingredient ing = ingredientService.getIngredient(id);
        if(ing != null) {
            return new ResponseEntity<>(ing, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/ingredients")
    public ResponseEntity<Ingredient> addIngredient(@RequestBody Ingredient ingredient) {
        try {
            ingredientService.addIngredient(ingredient);
            return new ResponseEntity<>(ingredient, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PatchMapping(value = "/ingredient/{id}")
    public ResponseEntity<Ingredient> updateIngredient(@PathVariable Integer id, @RequestBody Ingredient ingredient) {
        Ingredient ing = ingredientService.getIngredient(id);

        if (ing == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        ing.setIngredient_amount((ingredient.getIngredient_amount() != null) ?
                ingredient.getIngredient_amount():
                ing.getIngredient_amount());

        ing.setIngredient_type((ingredient.getIngredient_type() != null) ?
                ingredient.getIngredient_type():
                ing.getIngredient_type());

        ingredientService.updateIngredient(ing);

        return new ResponseEntity<>(ing, HttpStatus.OK);
    }

    @DeleteMapping(value = "/ingredient/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        Ingredient ing = ingredientService.getIngredient(id);

        if (ing == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        ingredientService.deleteIngredient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
