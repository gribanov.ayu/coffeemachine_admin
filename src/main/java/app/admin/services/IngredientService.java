package app.admin.services;

import app.admin.dao.IIngredientDao;
import app.admin.dao.IngredientDao;
import app.admin.models.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@Service("ingredientService")
@EnableTransactionManagement
public class IngredientService {
    private final IIngredientDao ingredientDao;

    @Autowired
    public IngredientService(IngredientDao ingredientDao) {
        this.ingredientDao = ingredientDao;
    }

    @Transactional
    public Ingredient getIngredient(int id) {
        return ingredientDao.read(id);
    }

    @Transactional
    public void addIngredient(Ingredient ingredient) {
        ingredientDao.create(ingredient);
    }

    @Transactional
    public void updateIngredient(Ingredient ingredient) {
        ingredientDao.update(ingredient);
    }

    @Transactional
    public void deleteIngredient(int id) {
        ingredientDao.delete(id);
    }
}
