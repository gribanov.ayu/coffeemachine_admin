package app.admin.services;

import app.admin.dao.GlassDao;
import app.admin.dao.IGlassDao;
import app.admin.models.Glass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@Service("glassService")
@EnableTransactionManagement
public class GlassService {
    private final IGlassDao glassDao;

    @Autowired
    public GlassService(GlassDao glassDao) {
        this.glassDao = glassDao;
    }

    @Transactional
    public Glass getGlass(int id) {
        return glassDao.read(id);
    }

    @Transactional
    public void addGlass(Glass glass) {
        glassDao.create(glass);
    }

    @Transactional
    public void updateGlass(Glass glass) {
        glassDao.update(glass);
    }

    @Transactional
    public void deleteGlass(int id) {
        glassDao.delete(id);
    }
}
