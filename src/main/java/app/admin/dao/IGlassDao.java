package app.admin.dao;

import app.admin.models.Glass;

public interface IGlassDao {
    void create(Glass ingredient);

    Glass read(int id);

    void update(Glass ingredient);

    void delete(int id);
}
