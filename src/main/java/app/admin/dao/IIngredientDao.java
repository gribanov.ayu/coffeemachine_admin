package app.admin.dao;

import app.admin.models.Ingredient;

public interface IIngredientDao {
    void create(Ingredient ingredient);

    Ingredient read(int id);

    void update(Ingredient ingredient);

    void delete(int id);
}
