package app.admin.dao;

import app.admin.models.Ingredient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class IngredientDao implements IIngredientDao {
    private final SessionFactory sessionFactory;

    @Autowired
    public IngredientDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Ingredient ingredient) {
        Session session = sessionFactory.getCurrentSession();
        session.save(ingredient);
    }

    @Override
    public Ingredient read(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.get(Ingredient.class, id);
    }

    @Override
    public void update(Ingredient ingredient) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(ingredient);
    }

    @Override
    public void delete(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(this.read(id));
    }
}
