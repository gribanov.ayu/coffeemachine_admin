package app.admin.dao;

import app.admin.models.Glass;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GlassDao implements IGlassDao {
    private final SessionFactory sessionFactory;

    @Autowired
    public GlassDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Glass glass) {
        Session session = sessionFactory.getCurrentSession();
        session.save(glass);
    }

    @Override
    public Glass read(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.get(Glass.class, id);
    }

    @Override
    public void update(Glass glass) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(glass);
    }

    @Override
    public void delete(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(this.read(id));
    }
}
